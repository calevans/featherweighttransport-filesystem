# Featherweight Queue Trasnport - Filesystem<br />
Cal Evans <cal@calevans.com>

This is the file system transport for Featherweight Queue. I do not recommend using this transport in production. It is mainly for development.

# Configuration

Add these entries to your `.env` file.

```
TRANSPORT_FILESYSTEM_ROOT_QUEUE_PATH=queues/
```

If you are running this as a standalone inside a seprate Lando environment from the server, make sure you map a directory from the server's queue directory to your local one.

```
services:
  appserver:
    overrides:
      volumes:
        - /home/your_user_name/Projects/featherweight-queue/featherweight-queue-server/queues:/app/queues
```