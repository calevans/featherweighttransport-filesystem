<?php

declare(strict_types=1);

namespace Eicc\Fwq\Transport\FileSystem;

use Eicc\Fwq\Exceptions\InvalidQueueException;
use Eicc\Fwq\Exceptions\QueueEmptyException;
use Eicc\Fwq\Models\AbstractTransport;
use Pimple\Container;
use Ramsey\Uuid\Uuid;

/**
 * This is the FileSystem Trasnport. It is designed for development use only.
 */
class Transport extends AbstractTransport
{
  public const JOB_EXTENSION = 'job';
  public const META_FILE_NAME = '.meta';

  protected ?Container $container;

  public function __construct(Container $container)
  {
    $paths = $container['paths'];
    $paths['filesystemqueue'] = $container['paths']['root'] . $_ENV['TRANSPORT_FILESYSTEM_ROOT_QUEUE_PATH'];
    $container['paths'] = $paths;
    $this->container = $container;
  }

  public function push(object $luw, \DateTimeImmutable $runAfter, string $queueName): void
  {
    if (! $this->doesQueueExist($queueName)) {
      $this->container['log']->error($queueName . ' is an invlaid queue name.');
      throw new InvalidQueueException($queueName . ' is an invlaid queue name.');
    }

    $uuid = Uuid::uuid4();
    $fileName = $uuid . '.' . self::JOB_EXTENSION;

    $luw->jobId = $uuid;
    $luw->queueName = $queueName;
    $luw->runAfter = $runAfter->format($_ENV['DATE_FORMAT']);
    $jsonEncodedLUW = json_encode($luw);

    $runAfterUnixTime = (int)$runAfter->format('U');
    $fullPathAndFileName = $this->getQueuePath($queueName) . $fileName;
    file_put_contents($fullPathAndFileName, $jsonEncodedLUW);
    touch($fullPathAndFileName, $runAfterUnixTime);
  }

  public function pop(string $queueName, string $workerName): object|null
  {
    $this->container['log']->debug('TRANSPORT: BEGIN POPPING a job for ' . $queueName . ':' . $workerName);

    if (! $this->doesQueueExist($queueName)) {
      $this->container['log']->error($queueName . ' is an invlaid queue name.');
      throw new InvalidQueueException($queueName . ' is an invlaid queue name.');
    }

    $queue = $this->getQueuePath($queueName);

    $jobList = $this->jobList($queueName);
    $today = new \DateTimeImmutable();

    while (! empty($jobList)) {
      $keys = array_keys($jobList);
      $selectedJobKey = $keys[0];

      $fileDate = new \DateTimeImmutable($jobList[$selectedJobKey]);

      if ($fileDate > $today) {
        unset($jobList[$selectedJobKey]);
        continue;
      }

      $selectedJob = ['fileName' => $selectedJobKey,'runAfter' => $jobList[$selectedJobKey]];
      unset($jobList[$selectedJobKey]);

      $luw = json_decode(file_get_contents($queue . $selectedJob['fileName']));

      if (! $this->lockFile($queue . $selectedJob['fileName'], $workerName)) {
        $this->container['log']->debug('Failed  to lock ' . $selectedJob['fileName']);
        continue;
      }

      $this->container['log']->debug('END POPPING a job for ' . $queueName);

      return $luw;
    }

    return null;
  }


  public function jobCount(string $queueName): int
  {
    $this->container['log']->debug('TRANSPORT: counting jobs for ' . $queueName);
    return count($this->jobList($queueName));
  }

  protected function getQueuePath(string $queueName): string
  {
    return $this->container['paths']['filesystemqueue'] . strtolower($queueName) . '/';
  }

  /**
   * Return an array of the jobs currently in the specified queue.
   *
   * This does not recurse directories on purpose.
   */
  public function jobList(string $queueName): array
  {
    $this->container['log']->debug('TRANSPORT: BEGIN Fetching jobs for ' . $queueName);
    $returnValue = [];
    $queuePath = $this->getQueuePath($queueName);
    $queue = dir($queuePath);

    while (false !== ($entry = $queue->read())) {
      if ((pathinfo($entry)['extension'] ?? '') !== self::JOB_EXTENSION) {
        continue;
      }

      $returnValue[$entry] = date('Y-m-d H:i:s', filemtime($queuePath . $entry));
    }

    switch ($this->readMeta($queueName)->direction) {
      case 'LIFO':
        arsort($returnValue);
          break;
      case 'FIFO':
      default:
        asort($returnValue);
    }

    $this->container['log']->debug('TRANSPORT: END Fetching jobs for ' . $queueName, [count($returnValue)]);
    return $returnValue;
  }

  public function getSetting(string $queueName, string $key)
  {
    $meta = $this->readMeta($queueName);
    if (! isset($meta->$key)) {
      $this->container['log']->error($key . ' is not a valid setting.');
      throw new \Exception($key . ' is not a valid setting.');
    }

    return $meta->$key;
  }

  public function setSetting(string $queueName, string $key, $value): void
  {
    $meta = $this->readMeta($queueName);
    if (! isset($meta->$key)) {
      $this->container['log']->error($key . ' is not a valid setting.');
      throw new \Exception($key . ' is not a valid setting.');
    }
    $meta->$key = $value;
    $metaFile = $this->getQueuePath($queueName) . self::META_FILE_NAME;

    file_put_contents($metaFile, json_encode($meta));
  }

  public function createQueue(string $queueName, array $parameters): void
  {
    if (file_exists($this->getQueuePath($queueName))) {
      $this->container['log']->error($queueName . ' already exists.');
      throw new \Exception($queueName . ' already exists.');
    }

    mkdir($this->getQueuePath($queueName), 0777, true);

    $metaFile = $this->getQueuePath($queueName) . self::META_FILE_NAME;
    $meta = $this->makeMeta($queueName);
    $meta->direction = $parameters['direction'] ?? 'FIFO';
    file_put_contents($metaFile, json_encode($meta));
  }

  public function destroyQueue(string $queueName): void
  {
    if (! $this->doesQueueExist($queueName)) {
      $this->container['log']->error($queueName . ' is not a valid queue name.');
      throw new InvalidQueueException($queueName . ' is not a valid queue name.');
    }

    if ($this->jobCount($queueName) > 0) {
      $this->container['log']->error($queueName . ' is not empty thus cannot be destroyed.');
      throw new QueueEmptyException($queueName . ' is not empty thus cannot be destroyed.');
    }

    $this->recursiveDirectoryDelete($this->getQueuePath($queueName));
  }

  /**
   * Check to see if the given queue name is a valid queue.
   */
  public function doesQueueExist(string $queueName): bool
  {
    return file_exists($this->getQueuePath($queueName));
  }

  /**
   * Returns an array of the queues in the system
   */
  public function listQueues(): array
  {
    $listOfQueues = [];

    $dir = dir($this->container['paths']['filesystemqueue']);
    while (false !== ($entry = $dir->read())) {
      if ($entry === '.' || $entry === '..' || ! is_dir($this->container['paths']['filesystemqueue'] . $entry)) {
        continue;
      }

      if (! file_exists($this->container['paths']['filesystemqueue'] . $entry  . '/' . self::META_FILE_NAME)) {
        continue;
      }

      $listOfQueues[] = $entry;
    }
    $dir->close();

    return $listOfQueues;
  }

  public function getMeta(string $queueName): object
  {
    if (! $this->doesQueueExist($queueName)) {
      throw new InvalidQueueException($queueName . ' is not a valid queue name.');
    }

    return $this->readMeta($queueName);
  }

  public function initalize(): void
  {
    if (! is_dir($this->container['paths']['filesystemqueue'])) {
      mkdir($this->container['paths']['filesystemqueue'], 0777, true);
    }

    if (! file_exists($this->getQueuePath($_ENV['FAILED_JOB_QUEUE']))) {
      $this->createQueue($_ENV['FAILED_JOB_QUEUE'], ['direction' => 'FIFO']);
    }
    if (! file_exists($this->getQueuePath('low_priority'))) {
      $this->createQueue('low_priority', ['direction' => 'FIFO']);
    }
  }


  /*
   * METHODS NOT PART OF THE INTERFACE
   */

  /**
   * Lock a job for working.
   *
   * In this transport, locking the job actually removes the file. If it fails,
   * it can be requeued. It does rename it first to make sure it has exclusive
   * access to it. If the rename fails it's because someone else got to it
   * first. It will return false.
   */
  protected function lockFile(string $fileName, string $workerName): bool
  {
    $filenameParts = pathinfo($fileName);
    $filenameParts['extension'] = $workerName;
    $newFileName = $filenameParts['dirname'] . '/'
    . $filenameParts['filename']
    . '.' . $filenameParts['basename'];
    if (rename($fileName, $newFileName)) {
      unlink($newFileName);
      return true;
    }
    return false;
  }

  /**
   * Read in the meta information for a queue and return it as an object.
   */
  protected function readMeta(string $queueName): object
  {
    $queuePath = $this->getQueuePath($queueName);
    $metaFile = $queuePath . self::META_FILE_NAME;

    if (! file_exists($metaFile)) {
      $this->container['log']->error($metaFile . ' does not exist.');
      throw new \Exception($metaFile . 'does not exist.');
    }

    $rawJson = file_get_contents($metaFile);
    return json_decode($rawJson);
  }

  /**
   * Any new meta data that needs to be tracked, add it here.
   */
  protected function makeMeta(string $queueName): object
  {
    $returnValue = new \StdClass();

    $returnValue->name = $queueName;

    foreach ($this->meta as $meta => $defaultValue) {
      $returnValue->$meta = $defaultValue;
    }

    return $returnValue;
  }


  protected function recursiveDirectoryDelete($dir)
  {
    $files = array_diff(scandir($dir), array('.','..'));
    foreach ($files as $file) {
      (is_dir("$dir/$file")) ? $this->recursiveDirectoryDelete("$dir/$file") : unlink("$dir/$file");
    }

    return rmdir($dir);
  }
}
